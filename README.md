# Personalized API providing formatted data for my Raspberry Pi eInk display

This repository will host a handful of different API endpoints that allow my Raspberry Pi to request specific information which it will present on the eInk display it is linked to. As of now, the API has the following endpoints:

## Formula 1 API Endpoints
This API provides information about Formula 1 sessions and grand prix for the 2024 season.

### Endpoints

```GET /api/formula1/2024/sessions```

* Returns all sessions for the 2024 season.

* Response: An array of session objects, each containing the grand prix name, session name, date, and time. The sessions are ordered ascending in time.


```GET /api/formula1/2024/grand-prix```

* Returns all grand prix for the 2024 season.

* Response: An array of grand prix objects, each containing the grand prix name, an array of its sessions, and the track information. The sessions within each grand prix are ordered ascending in time.


```GET /api/formula1/2024/grand-prix/upcoming```

* Returns the upcoming grand prix including individual sessions and weather forecast if available (next 5 days).

* Response: A grand prix object containing the grand prix name, an array of its sessions, and the track information. Each session includes a forecast object if a forecast is available. The sessions are ordered ascending in time.


```GET /api/formula1/2024/grand-prix/:name```

* Returns the grand prix including individual sessions by grand prix name.

* Parameters: :name - The name of the grand prix (e.g., belgian for the Belgian Grand Prix).

* Response: A grand prix object containing the grand prix name, an array of its sessions, and the track information. The sessions are ordered ascending in time.

* Error: If the grand prix name is not found, a 404 status code is returned with a message explaining how to correctly provide the grand prix name.