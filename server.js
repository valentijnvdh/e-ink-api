require('dotenv').config();
const express = require('express');
const app = express();
const axios = require('axios');
const fs = require('fs');
const path = require('path');
const markdownIt = require('markdown-it');
const md = markdownIt();

const developmentMode = process.env.NODE_ENV != 'production';
const port = process.env.PORT || 5000;

async function getForecast3(lat, lon, timestamp) {
    // Get the forecast from OpenWeather API
    const response = await axios.get(`https://api.openweathermap.org/data/3.0/onecall/timemachine?lat=${lat}&lon=${lon}&dt=${timestamp}&appid=${process.env.OPENWEATHER_API_KEY}`);
    const forecast = response.data;
    return forecast;
}

async function getForecast2_5(lat, lon) {
    // Get the forecast from OpenWeather API
    const response = await axios.get(`https://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&appid=${process.env.OPENWEATHER_API_KEY}`);
    const forecast = response.data;
    return forecast;
}

app.get('/', (req, res) => {
    const readme = fs.readFileSync('./README.md', 'utf8');
    const result = md.render(readme);
    res.send(result);
});

// Returns all sessions for the season of 2024
app.get('/api/formula1/2024/sessions', async (req, res) => {

    // Get the current timestamp unix in seconds
    const timestamp = Math.floor(Date.now() / 1000);

    // From /data/formula1/2024/sessions.json, get all the sessions
    const sessions = JSON.parse(fs.readFileSync(path.join(__dirname, 'data', 'formula1', '2024', 'sessions.json')));
    // Make sure the sessions are ordered ascending in time
    sessions.sort((a, b) => a.timestamp - b.timestamp);
    
    // Send the response to the client
    res.send(sessions);
});

// Returns all grand-prix for the season of 2024
app.get('/api/formula1/2024/grand-prix', async (req, res) => {

    // From /data/formula1/2024/sessions.json, get all the sessions
    const sessions = JSON.parse(fs.readFileSync(path.join(__dirname, 'data', 'formula1', '2024', 'sessions.json')));
    // Make sure the sessions are ordered ascending in time
    sessions.sort((a, b) => a.timestamp - b.timestamp);

    // Get all the unique grand prix names from the sessions
    const grandPrixNames = [...new Set(sessions.map(session => session.grand_prix))];
    // Create an array of grand prix objects (grandPrix) with the grand prix name and sessions
    const allGrandPrix = grandPrixNames.map(grand_prix => {
        const grandPrixSessions = sessions.filter(session => session.grand_prix == grand_prix);
        grandPrixSessions.sort((a, b) => a.timestamp - b.timestamp);
        return {
            grand_prix,
            sessions: grandPrixSessions
        };
    });

    // Using the tracks.json file, get the track for each grand prix and add it to the grand prix object
    const tracks = JSON.parse(fs.readFileSync(path.join(__dirname, 'data', 'formula1', 'tracks.json')));
    allGrandPrix.forEach(grandPrix => {
        const track = tracks.find(track => track.grand_prix == grandPrix.grand_prix);
        grandPrix.track = track;
    });

    // Send the response to the client
    res.send(allGrandPrix);

});

// Returns the upcoming grand-prix including individual sessions and weather forecast if available (next 5 days)
app.get('/api/formula1/2024/grand-prix/upcoming', async (req, res) => {

    // Get the current timestamp unix in seconds
    const timestamp = Math.floor(Date.now() / 1000);
    // Get the unix timestamp in seconds 70 days from now
    const seventheeDaysFromNow = timestamp + 70 * 24 * 60 * 60;

    // From /data/formula1/2024/sessions.json, get all the sessions
    const sessions = JSON.parse(fs.readFileSync(path.join(__dirname, 'data', 'formula1', '2024', 'sessions.json')));
    // Make sure the sessions are ordered ascending in time
    sessions.sort((a, b) => a.timestamp - b.timestamp);
    // Identify the first session that falls within the next 70 days
    const nextSession = sessions.find(session => session.timestamp > timestamp && session.timestamp < seventheeDaysFromNow);
    // Get the grand prix name of the session
    const grand_prix = nextSession.grand_prix;
    // Get all the sessions for the grand prix
    const grandPrixSessions = sessions.filter(session => session.grand_prix == grand_prix);
    // Make sure they are order ascending in time
    grandPrixSessions.sort((a, b) => a.timestamp - b.timestamp);

    // Create a grand prix object (grandPrix) with the grand prix name and sessions
    const grandPrix = {
        grand_prix,
        sessions: grandPrixSessions
    };

    // Using the tracks.json file, get the track for the grand prix and add it to the grand prix object
    const tracks = JSON.parse(fs.readFileSync(path.join(__dirname, 'data', 'formula1', 'tracks.json')));
    const track = tracks.find(track => track.grand_prix == grand_prix);
    grandPrix.track = track;

    // For each session in the grand prix, get the forecast and add it to the session
    for (let session of grandPrix.sessions) {
        var forecast = await getForecast2_5(grandPrix.track.coordinates.lat, grandPrix.track.coordinates.lon);
        // Get the weather (forecast.list[*].weather) from the timeblock in the list (forecast.list) that has a timestamp (obj.list.dt) closest to the session timestamp, as long as its within 5 hours of the timestamp
        const timeblock = forecast.list.find(timeblock => Math.abs(timeblock.dt - session.timestamp) < 3 * 60 * 60);
        // If no timeblock is found, set the forecast to null and continue to the next session
        if (!timeblock) {
            session.forecast = null;
            continue;
        }
        // Add the forecast to the session
        session.forecast = { 
            details: timeblock
        };
        // If the weather array contains an object with main = 'Rain', add a rain = true property to the session
        session.forecast.rain = session.forecast.details.weather.some(weather => weather.main == 'Rain');
        // If the weather array contains an object with main = 'Drizzle', add a drizzle = true property to the session
        session.forecast.drizzle = session.forecast.details.weather.some(weather => weather.main == 'Drizzle');
        // If the weather array contains an objec with main = 'Snow', add a snow = true property to the session
        session.forecast.snow = session.forecast.details.weather.some(weather => weather.main == 'Snow');
        // If the weather array contains an object with main = 'Thunderstorm', add a thunderstorm = true property to the session
        session.forecast.thunderstorm = session.forecast.details.weather.some(weather => weather.main == 'Thunderstorm');
        // If the weather array contains an object with main = 'Clear', add a clear = true property to the session
        session.forecast.clear = session.forecast.details.weather.some(weather => weather.main == 'Clear');
        // If the weather array contains an object with main = 'Clouds', add a clouds = true property to the session
        session.forecast.clouds = session.forecast.details.weather.some(weather => weather.main == 'Clouds');

    }

    // Send the response to the client
    res.send(grandPrix);
});

// Returns the grand prix including individual sessions by grand prix name
app.get('/api/formula1/2024/grand-prix/:name', async (req, res) => {

    // Get the grand prix name from the request and uppercase the first letter
    const grand_prix_name = req.params.name.charAt(0).toUpperCase() + req.params.name.slice(1);

    // Create the full grand prix name by adding ' Grand Prix' to the grand prix name	
    const grand_prix = grand_prix_name + ' Grand Prix';

    // From /data/formula1/2024/sessions.json, get all the sessions
    const sessions = JSON.parse(fs.readFileSync(path.join(__dirname, 'data', 'formula1', '2024', 'sessions.json')));

    // Create a grand prix object (grandPrix) with the grand prix name and sessions
    const grandPrix = {
        grand_prix: grand_prix,
        sessions: sessions.filter(session => session.grand_prix == grand_prix)
    };

    if (grandPrix.sessions.length == 0) {
        res.status(404).send('Grand Prix not found. Be sure to provide the name of the grand prix as follows: if you want to find the Belgian Grand Prix, use the endpoint /api/formula1/2024/grand-prix/belgian. Note that its not the exact country name (i.e. Belgium) but rather the grand prix name form (similar to how the Grand Prix in Great Britian is called the "British" Grand Prix. Also, the name should be in lowercase.');
        return;
    }

    // Make sure the sessions are ordered ascending in time
    grandPrix.sessions.sort((a, b) => a.timestamp - b.timestamp);

    // Using the tracks.json file, get the track for the grand prix and add it to the grand prix object
    const tracks = JSON.parse(fs.readFileSync(path.join(__dirname, 'data', 'formula1', 'tracks.json')));
    const track = tracks.find(track => track.grand_prix == grand_prix);
    grandPrix.track = track;

    // Send the response to the client
    res.send(grandPrix);
});

// Initiate the app at port
app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});